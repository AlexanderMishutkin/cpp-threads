cmake_minimum_required(VERSION 3.16)
project(ABC)

set(CMAKE_CXX_STANDARD 17)

add_executable(ABC main.cpp)
find_package(Threads REQUIRED)
target_link_libraries(ABC ${CMAKE_THREAD_LIBS_INIT})