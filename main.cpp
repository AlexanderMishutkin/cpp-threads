/*
 * Мишуткин Александр, БПИ-192
 * Вариант-16
 * 15.11.2020
 *
 * Задача об инвентаризации по рядам. После нового года в
 * библиотеке университета обнаружилась пропажа каталога. После поиска и
 * наказания виноватых, ректор дал указание восстановить каталог силами
 * студентов. Фонд библиотека представляет собой прямоугольное помещение,
 * в котором находится M рядов по N шкафов по K книг в каждом шкафу.
 * Требуется создать многопоточное приложение, составляющее каталог. При
 * решении задачи использовать метод «портфель задач», причем в качестве
 * отдельной задачи задается составление каталога одним студентом для одного
 * ряда.
 */



#include <iostream>
#include <thread>
#include <vector>
#include <memory>
#include <sstream>
#include <ctime>
#include <string>
#include <random>
#include <mutex>
#include <chrono>

const int thread_number = 4;

struct Task {
    size_t raw;
    size_t catalog_begin;

    Task(size_t raw, size_t catalog_begin) : raw(raw), catalog_begin(catalog_begin) {
    }
};

class Portfolio {
public:
    std::mutex _lock;
    std::vector<Task> _tasks;

    explicit Portfolio(std::vector<Task> &&tasks) : _tasks(tasks) {
    }

    bool TryGetTask(const std::shared_ptr<Task> &task) {
        _lock.lock();

        if (_tasks.empty()) {
            _lock.unlock();
            return false;
        }

        *task = _tasks.back();
        _tasks.pop_back(); // critical
        _lock.unlock();
        return true;
    }
};

std::string GenerateString(std::mt19937 &gen) {
    std::string string(gen() % 20 + 5, '\0');
    for (size_t i = 0; i < string.size(); i++) {
        if (gen() % 7 < 6 || i == 0 || string[i - 1] == ' ') {
            string[i] = gen() % 26 + 'a';
        } else {
            string[i] = ' ';
        }

        if (i == 0) {
            string[i] += 'A' - 'a';
        } else {
            if (string[i - 1] == ' ' && gen() % 10 < 9) {
                string[i] += 'A' - 'a';
            }
        }
    }

    return std::move(string);
}

std::shared_ptr<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]> GenerateLibrary(size_t M, size_t N, size_t K) {
    std::mt19937 gen(std::time(nullptr) * 53);

    // Yes, it's not so pretty code, but it is done in best cpp traditions ))
    std::shared_ptr<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]> library(
            new std::shared_ptr<std::shared_ptr<std::string[]>[]>[M],
            std::default_delete<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]>());

    for (size_t raw = 0; raw < M; raw++) {

        library[raw] = std::shared_ptr<std::shared_ptr<std::string[]>[]>(new std::shared_ptr<std::string[]>[N],
                                                                         std::default_delete<std::shared_ptr<std::string[]>[]>());

        for (size_t book_case = 0; book_case < N; book_case++) {

            library[raw][book_case] = std::shared_ptr<std::string[]>(new std::string[K],
                                                                     std::default_delete<std::string[]>());

            for (size_t book = 0; book < K; book++) {
                library[raw][book_case][book] = GenerateString(gen);
            }
        }
    }

    if (N * M * K < 100) {
        std::cout << "Сгенерирована библиотека:" << std::endl;
        for (size_t raw = 0; raw < M; raw++) {
            std::cout << " - Ряд: " << raw + 1 << std::endl;
            for (size_t book_case = 0; book_case < N; book_case++) {
                std::cout << "   - Шкаф: " << book_case + 1 << std::endl;
                for (size_t book = 0; book < K; book++) {
                    std::cout << "     - " << library[raw][book_case][book] << std::endl;
                }
            }
        }
    } else {
        std::cout << "Библиотека сгенерирована, но слишком велика для вывода" << std::endl;
    }

    return std::move(library);
}

std::shared_ptr<std::string[]>
CommonSolution(const std::shared_ptr<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]> &library, size_t M, size_t N,
               size_t K) {
    std::shared_ptr<std::string[]> catalog(new std::string[M * N * K], std::default_delete<std::string[]>());

    size_t cnt = 0;
    for (size_t raw = 0; raw < M; raw++) {
        for (size_t book_case = 0; book_case < N; book_case++) {
            for (size_t book = 0; book < K; book++) {
                std::stringstream stream;
                stream << "raw " << raw << ", book case " << book_case << ", name \"" << library[raw][book_case][book]
                       << "\"";

                catalog[cnt++] = stream.str();
            }
        }
    }

    return std::move(catalog);
}

void ListRaw(std::shared_ptr<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]> library,
             std::shared_ptr<Portfolio> portfolio,
             std::shared_ptr<std::string[]> catalog,
             size_t N,
             size_t K) {
    std::shared_ptr<Task> task = std::make_shared<Task>(0, 0);
    while (portfolio->TryGetTask(task)) {
        size_t cnt = task->catalog_begin;
        size_t raw = task->raw;

        for (size_t book_case = 0; book_case < N; book_case++) {
            for (size_t book = 0; book < K; book++) {
                std::stringstream stream;
                stream << "raw " << raw << ", book case " << book_case << ", name \"" << library[raw][book_case][book]
                       << "\"";

                catalog[cnt++] = stream.str();
            }
        }
    }
}

std::shared_ptr<std::string[]>
MultiThreadSolution(std::shared_ptr<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]> library, size_t M,
                    size_t N,
                    size_t K) {
    std::vector<std::thread> threads;
    std::vector<Task> task_list;
    std::shared_ptr<std::string[]> result(new std::string[N * M * K], std::default_delete<std::string[]>());

    size_t cnt = 0;
    for (size_t raw = 0; raw < M; raw++) {
        task_list.emplace_back(raw, cnt);
        cnt += N * K;
    }

    std::shared_ptr<Portfolio> portfolio = std::make_shared<Portfolio>(std::move(task_list));

    threads.reserve(thread_number);
    for (size_t i = 0; i < thread_number; i++) {
        threads.emplace_back(ListRaw, library, portfolio, result, N, K);
    }

    for (size_t i = 0; i < thread_number; i++) {
        threads[i].join();
    }

    return std::move(result);
}

int main() {
    int M, N, K;
    // M рядов по N шкафов по K книг в каждом шкафу
    std::cin >> M >> N >> K;

    while (M <= 0 || N <= 0 || K <= 0) {
        std::cout << "Размеры библиотеки заданы некорректно, введите еще раз:" << std::endl;
        std::cin >> M >> N >> K;
    }

    std::shared_ptr<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]> library = GenerateLibrary(M, N, K);

    auto start = std::chrono::high_resolution_clock::now();
    auto res = CommonSolution(library, M, N, K);
    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "Обычный алгоритм справился за          "
              << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count() << " нс"
              << std::endl;

    auto start_mt = std::chrono::high_resolution_clock::now();
    auto res_mt = MultiThreadSolution(library, M, N, K);
    auto end_mt = std::chrono::high_resolution_clock::now();
    std::cout << "Многопоточный алгоритм справился за    "
              << std::chrono::duration_cast<std::chrono::nanoseconds>(end_mt - start_mt).count() << " нс"
              << std::endl;

    if (M * N * K < 100) {
        std::cout << "Каталог обыного решения:" << std::endl;
        for (size_t i = 0; i < M * K * N; i++) std::cout << "  " << res[i] << std::endl;
        std::cout << "Каталог многопоточного решения:" << std::endl;
        for (size_t i = 0; i < M * K * N; i++) std::cout << "  " << res_mt[i] << std::endl;
    } else {
        std::cout << "Каталог слишком большой для вывода";
    }
    return 0;
}
